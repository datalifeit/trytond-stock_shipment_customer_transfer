=========================================
Stock Shipment Customer Transfer Scenario
=========================================

Imports::

    >>> import datetime
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from decimal import Decimal
    >>> today = datetime.date.today()

Install stock_shipment_customer_transfer::

    >>> config = activate_modules('stock_shipment_customer_transfer')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products

Get customer location::

    >>> Location = Model.get('stock.location')
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> customer_loc2, = customer_loc.duplicate()

Create an internal shipment with customer location::

    >>> ShipmentInternal = Model.get('stock.shipment.internal')
    >>> shipment_internal = ShipmentInternal()
    >>> shipment_internal.planned_date = today
    >>> shipment_internal.company = company
    >>> shipment_internal.from_location = customer_loc
    >>> shipment_internal.to_location = customer_loc2
    >>> shipment_internal.save()
    >>> move = shipment_internal.moves.new()
    >>> move.from_location = customer_loc
    >>> move.to_location = customer_loc2
    >>> move.product = product
    >>> move.quantity = 1
    >>> shipment_internal.click('wait')
    >>> shipment_internal.click('assign_try')
    True
    >>> shipment_internal.click('done')
