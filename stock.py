# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta

__all__ = ['ShipmentInternal']


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    @classmethod
    def __setup__(cls):
        super(ShipmentInternal, cls).__setup__()
        cls.from_location.domain = ['OR',
                cls.from_location.domain,
                ('type', '=', 'customer'),
        ]
        cls.to_location.domain = ['OR',
                cls.to_location.domain,
                ('type', '=', 'customer'),
        ]
